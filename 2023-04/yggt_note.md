# Git 使用注意事项

## 安装

安装完成后，还需要最后一步设置，在命令行输入：

```bash
git config --global user.name "Your Name"
git config --global user.email "email@example.com"
```

## `git add .`,`git add -A`与`git add -u`的区别

git add -A和 git add .   git add -u在功能上看似很相近，但还是存在一点差别

git add . ：他会监控工作区的状态树，使用它会把工作时的所有变化提交到暂存区，包括文件内容修改(modified)以及新文件(new)，但不包括被删除的文件。

git add -u ：他仅监控已经被add的文件（即tracked file），他会将被修改的文件提交到暂存区。add -u 不会提交新文件（untracked file）。（git add --update的缩写）

git add -A ：是上面两个功能的合集（git add --all的缩写）

> ### 总结
>
> * ___git add -A  提交所有变化___
> * ___git add -u  提交被修改(modified)和被删除(deleted)文 件，不包括新文件(new)___
> * ___git add .  提交新文件(new)和被修改(modified)文件，不包括被删除(deleted)文件___

## 忽略特殊文件

把指定文件排除在`.gitignore`规则外的写法就是 __!__ +文件名，所以，只需把例外文件添加进去即可。

## 配置文件

配置Git的时候，加上`--global`是针对当前用户起作用的，如果不加，那只针对当前的仓库起作用。

配置文件放哪了？每个仓库的Git配置文件都放在 __.git/config__ 文件中。

别名就在 __[alias]__ 后面，要删除别名，直接把对应的行删掉即可。

而当前用户的Git配置文件放在用户主目录下的一个隐藏文件 __.gitconfig__ 中。

配置别名也可以直接修改这个文件，如果改错了，可以删掉文件重新通过命令配置。

## 常用命令总结

* 初始化一个Git仓库，使用`git init`命令。

* 添加文件到Git仓库，分两步：

    1. 使用命令`git add <file>`，注意，可反复多次使用，添加多个文件；

    2. 使用命令`git commit -m <message>`，完成。

* 要随时掌握工作区的状态，使用`git status`命令。

* 如果`git status`告诉你有文件被修改过，用`git diff`可以查看修改内容。

* __HEAD__ 指向的版本就是当前版本，因此，Git允许我们在版本的历史之间穿梭，使用命令`git reset --hard commit_id`。

* 穿梭前，用`git log`可以查看提交历史，以便确定要回退到哪个版本。

* 要重返未来，用`git reflog`查看命令历史，以便确定要回到未来的哪个版本。

* 撤销修改

    1. 当你改乱了工作区某个文件的内容，想直接丢弃工作区的修改时，用命令`git checkout -- file`。

    2. 当你不但改乱了工作区某个文件的内容，还添加到了暂存区时，想丢弃修改，分两步，第一步用命令`git reset HEAD <file>`，就回到了场景1，第二步按场景1操作。

    3. 已经提交了不合适的修改到版本库时，想要撤销本次提交，使用命令`git reset --hard commit_id`，不过前提是没有推送到远程库。

* 命令`git rm`用于删除一个文件。如果一个文件已经被提交到版本库，那么你永远不用担心误删，但是要小心，你只能恢复文件到最新版本，__你会丢失最近一次提交后你修改的内容__。

* 创建SSH Key。在用户主目录下，看看有没有 __.ssh__ 目录，如果有，再看看这个目录下有没有 __id_rsa__ 和 __id_rsa.pub__ 这两个文件，如果已经有了，可直接跳到下一步。如果没有，打开Shell（Windows下打开Git Bash），创建SSH Key：

    ```bash
    ssh-keygen -t rsa -C "youremail@example.com"
    ```

* 添加远程库

    要关联一个远程库，使用命令`git remote add origin git@server-name:path/repo-name.git`；

    关联一个远程库时必须给远程库指定一个名字，__origin__ 是默认习惯命名；

    关联后，使用命令`git push -u origin master`第一次推送master分支的所有内容；

    此后，每次本地提交后，只要有必要，就可以使用命令`git push origin master`推送最新修改；

    分布式版本系统的最大好处之一是在本地工作完全不需要考虑远程库的存在，也就是有没有联网都可以正常工作，而SVN在没有联网的时候是拒绝干活的！当有网络的时候，再把本地提交推送一下就完成了同步，真是太方便了！

* 从远程库克隆

    要克隆一个仓库，首先必须知道仓库的地址，然后使用`git clone`命令克隆。

    Git支持多种协议，包括 __https__，但 __ssh__ 协议速度最快。

* 创建与合并分支

    查看分支：`git branch`

    创建分支：`git branch <name>`

    切换分支：`git checkout <name>`或者`git switch <name>`

    创建+切换分支：`git checkout -b <name>`或者`git switch -c <name>`

    合并某分支到当前分支：`git merge <name>`

    删除分支：`git branch -d <name>`

* 用`git log --graph`命令可以看到分支合并图。

* 合并分支时，加上`--no-ff`参数就可以用普通模式合并，合并后的历史有分支，能看出来曾经做过合并，而 __fast forward__ 合并就看不出来曾经做过合并。

* Bug分支

    修复bug时，我们会通过创建新的bug分支进行修复，然后合并，最后删除；

    当手头工作没有完成时，先把工作现场`git stash`一下，然后去修复bug，修复后，再`git stash pop`，回到工作现场；

    在master分支上修复的bug，想要合并到当前dev分支，可以用`git cherry-pick <commit>`命令，把bug提交的修改“复制”到当前分支，避免重复劳动。

* 如果要丢弃一个没有被合并过的分支，可以通过`git branch -D <name>`强行删除。

* 多人协作

    查看远程库信息，使用`git remote -v`；

    本地新建的分支如果不推送到远程，对其他人就是 __不可见__ 的；

    从本地推送分支，使用`git push origin branch-name`，如果推送失败，先用`git pull`抓取远程的新提交；

    在本地创建和远程分支对应的分支，使用`git checkout -b branch-name origin/branch-name`，本地和远程分支的名称最好一致；

    建立本地分支和远程分支的关联，使用`git branch --set-upstream-to=origin/<branch-name> <branch-name>`；

    从远程抓取分支，使用`git pull`，如果有冲突，要先处理冲突。

* `git rebase`操作可以把本地未push的分叉提交历史整理成直线；`git rebase`的目的是使得我们在查看历史提交的变化时更容易，因为分叉的提交需要三方对比。

* 创建标签

    命令`git tag <tagname>`用于新建一个标签，默认为 __HEAD__，也可以指定一个commit id；

    命令`git tag -a <tagname> -m "blablabla..."`可以指定标签信息；

    命令`git tag`可以查看所有标签；

    命令`git show <tagname>`可以查看标签信息。

* 操作标签

    命令`git push origin <tagname>`可以推送一个本地标签；

    命令`git push origin --tags`可以推送全部未推送过的本地标签；

    命令`git tag -d <tagname>`可以删除一个本地标签；

    命令`git push origin :refs/tags/<tagname>`可以删除一个远程标签。
